const server = require('server');
const { get, post } = server.router;
const { json, header } = server.reply;

const cors = [
  ctx => header("Access-Control-Allow-Origin", "*"),
  ctx => header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"),
  ctx => ctx.method.toLowerCase() === 'options' ? 200 : false
];

// Launch server with options and a couple of routes
server({ port: 3000 ,security: { csrf: false },}, cors,[
  get('/movies', ctx => json(movies)),
  post('/movies', ctx => {
    console.log(ctx.data);
    movies.push(ctx.data);
    return 202;
  })
]);

const movies =[
  {
    name:"Sky Fall", id:1
  },

]
